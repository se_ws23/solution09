package observer.device

import observer.log.{Entity, EntityChange, Loggable, SensorChange}

import scala.util.Random

case class BrightnessSensor(name: String) extends Sensor with Loggable[Entity, EntityChange]:
  private var measure: Double = 0.0d

  override def currentMeasure: Double = measure
  
  override def remeasure(): Unit =
    val tmp = Random.between(0.0d, 1.0d+Double.MinPositiveValue)
    var delta: SensorChange = SensorChange.NoChange

    if (measure < tmp)
      delta = SensorChange.Increase(tmp-measure)
    else if (tmp < measure)
      delta = SensorChange.Decrease(measure-tmp)

    measure = tmp
    notifyObservers(this, delta)